// Grabbed the inputs by their IDs
const sku = document.getElementById("sku");
const objectName = document.getElementById("name");
const price = document.getElementById("price");

// Grabbed the ul
const listItems = document.getElementsByClassName("items-list");

// Grabbed the save button
const save = document.getElementById("btn");

// Added an eventListener on the save button with the 'click' trigger and a saveData() function
save.addEventListener("click", saveData);

// saveData function that creates a div with LIs that contain the inputs value
function saveData(event) {
  event.preventDefault();

  const checkbox = document.createElement("input");
  checkbox.setAttribute = ("type", "checkbox");
  checkbox.classList.add("delete-checkbox");

  const itemDiv = document.createElement("div");
  itemDiv.classList.add("item");

  const skuLi = document.createElement("li");
  skuLi.innerText = sku.value;
  skuLi.classList.add("item-li");

  const nameLi = document.createElement("li");
  nameLi.innerText = objectName.value;
  nameLi.classList.add("item-li");

  const priceLi = document.createElement("li");
  priceLi.innerText = price.value + "$";
  priceLi.classList.add("item-li");

  itemDiv.appendChild(skuLi);
  itemDiv.appendChild(nameLi);
  itemDiv.appendChild(priceLi);

  // Appending the div to the ul in product-page.php , which seems to throw an error and make the whole code not work
  listItems.appendChild(itemDiv);
}
