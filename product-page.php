<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product Page</title>
    <link rel="stylesheet" href="./styles/style-product.css">
</head>

<body>

    <!-- HEADER -->

    <header>
        <h1>Product Add</h1>
        <div class="buttons">
            <a href="./index.php"><button>Cancel</button></a>
        </div>
    </header>
    <div id="line"></div>

    <!-- FORM -->
    <?php

    $sku = "";
    $name = "";
    $price = "";
    $size = "";
    $height = "";
    $width = "";
    $length = "";
    $weight = "";
    $empty_error = "";
    $data_error = "";
    $empty = -1;
    $wrong_data = -1;

    // Here I validated every variable
    // If the field is empty, I incremented an $empty variable. If it exists at least one empty field
    // it will display the appropiate string. I used this method in order to not display 
    // the same string more than once.

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = trim(htmlspecialchars($_POST["name"]));
        if (empty($_POST["name"])) {
            $empty++;
        }
        if (gettype($name) !== "string") {
            $wrong_data++;
        }

        $sku = trim(htmlspecialchars($_POST["sku"]));
        if (empty($_POST["sku"])) {
            $empty++;
        }

        $price = trim(htmlspecialchars($_POST["price"]));
        if (empty($_POST["price"])) {
            $empty++;
        }
        if (gettype($price) !== "number" || $price < 0) {  // here i checked if the data type is correct, but also if the number is positive
            $wrong_data++;
        }

        // Depending on which option is selected the corresponding validation will work
        if ($_POST["dropdown"] == "dvd") {
            $size = trim(htmlspecialchars($_POST["size"]));
            if (empty($_POST["size"])) {
                $empty++;
            }
            if (gettype($size) !== "number" || $size < 0) {
                $wrong_data++;
            }
        } else if ($_POST["dropdown"] == "furniture") {
            $height = trim(htmlspecialchars($_POST["height"]));
            if (empty($_POST["height"])) {
                $empty++;
            }
            if (gettype($height) !== "number" || $height < 0) {
                $wrong_data++;
            }

            $length = trim(htmlspecialchars($_POST["length"]));
            if (empty($_POST["length"])) {
                $empty++;
            }
            if (gettype($length) !== "number" || $length < 0) {
                $wrong_data++;
            }

            $width = trim(htmlspecialchars($_POST["width"]));
            if (empty($_POST["width"])) {
                $empty++;
            }
            if (gettype($width) !== "number" || $width < 0) {
                $wrong_data++;
            }
        } else if ($_POST["dropdown"] == "book") {
            $weight = trim(htmlspecialchars($_POST["weight"]));
            if (empty($_POST["weight"])) {
                $empty++;
            }
            if (gettype($weight) !== "number" || $weight < 0) {
                $wrong_data++;
            }
        }

        if ($empty >= 0) {
            $empty_error .= "Please, submit required data <br>";
        }

        if ($wrong_data >= 0) {
            $data_error .= "Please, provide the data of indicated type";
        }
    }
    ?>

    <!-- FORMS -->

    <form method="post" action="" id="product_form">
        <p>
            SKU <input id="sku" type="text" name="sku" value="<?= $sku; ?>">
        </p>
        <p>
            Name <input type="text" name="name" id="name" value="<?= $name; ?>">
        </p>
        <p>
            Price ($) <input type="text" name="price" id="price" value="<?= $price; ?>">
        </p>
        <p>
            Type Switcher
            <select name="dropdown" id="productType" onchange="getSelectedValue();">
                <!-- I put a onchange event on the dropdown in order to fire the function to change 
                the input field corresponding the option selected -->
                <option value="default">Select an option</option>
                <option value="dvd">DVD</option>
                <option value="book">Book</option>
                <option value="furniture">Furniture</option>
            </select>

            <!-- Here are the options forms that have as default a display property of none -->
        <div id="sizeForm">
            <p>
                Size (MB) <input type="number" name="size" id="size" value="<?= $size; ?>">
            </p>
            <p>
                Please provide the size of the DVD in MB.
            </p>
        </div>

        <div id="furnitureForm">
            <p>
                Height (CM) <input type="number" name="height" id="height" value="<?= $height; ?>">
            </p>
            <p>
                Width (CM) <input type="number" name="width" id="width" value="<?= $width; ?>">
            </p>
            <p>
                Length (CM) <input type="number" name="length" id="length" value="<?= $length; ?>">
            </p>
            <p>
                Please provide dimensions in HxWxL format
            </p>
        </div>

        <div id="weightForm">
            <p>
                Weight (KG) <input type="number" name="weight" id="weight" value="<?= $weight; ?>">
            </p>
            <p>
                Please provide the weight in KG.
            </p>
        </div>
        <script>
            // First I grabbed every form by their id 
            const size = document.getElementById("sizeForm");
            const furniture = document.getElementById("furnitureForm");
            const weight = document.getElementById("weightForm");

            // Here I created a function that checks what value the select tag has and changes the display property
            // of the corresponding form
            function getSelectedValue() {
                let selectedValue = document.getElementById("productType").value;
                if (selectedValue === "dvd") {
                    size.style.display = "block";
                } else {
                    size.style.display = "none";
                }
                if (selectedValue === "book") {
                    weight.style.display = "block";
                } else {
                    weight.style.display = "none";
                }
                if (selectedValue === "furniture") {
                    furniture.style.display = "block";
                } else {
                    furniture.style.display = "none";
                }
            }
        </script>
        </p>
        <!-- Here are 2 span that appear in red text in order to notify the user of an error -->
        <span style="color: red"><?= $empty_error; ?></span>
        <span style="color: red"><?= $data_error; ?></span>

        <input type="submit" value="Save" action="./index.php" id="btn">
    </form>

    <!-- OPTIONS -->

    <!-- I creted a ul list to append with JS every input field from the forms to create the item -->
    <div class="items-container">
        <ul class="items-list"></ul>
    </div>


    <!-- FOOTER -->

    <footer>
        <h4>Scandiweb Test assignment</h4>
    </footer>


</body>
<script src="./main.js"></script>

</html>